/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duy.social.network.jpa.controller;

import com.duy.social.network.jpa.controller.exceptions.NonexistentEntityException;
import com.duy.social.network.jpa.controller.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.duy.social.network.jpa.entity.Article;
import com.duy.social.network.jpa.entity.Emotion;
import com.duy.social.network.jpa.entity.EmotionPK;
import com.duy.social.network.jpa.entity.Users;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author duy
 */
public class EmotionJpaController implements Serializable {

    public EmotionJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Emotion emotion) throws PreexistingEntityException, Exception {
        if (emotion.getEmotionPK() == null) {
            emotion.setEmotionPK(new EmotionPK());
        }
        emotion.getEmotionPK().setUserId(emotion.getUsers().getUserId());
        emotion.getEmotionPK().setArticleId(emotion.getArticle().getArticleId());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Article article = emotion.getArticle();
            if (article != null) {
                article = em.getReference(article.getClass(), article.getArticleId());
                emotion.setArticle(article);
            }
            Users users = emotion.getUsers();
            if (users != null) {
                users = em.getReference(users.getClass(), users.getUserId());
                emotion.setUsers(users);
            }
            em.persist(emotion);
            if (article != null) {
                article.getEmotionCollection().add(emotion);
                article = em.merge(article);
            }
            if (users != null) {
                users.getEmotionCollection().add(emotion);
                users = em.merge(users);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findEmotion(emotion.getEmotionPK()) != null) {
                throw new PreexistingEntityException("Emotion " + emotion + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Emotion emotion) throws NonexistentEntityException, Exception {
        emotion.getEmotionPK().setUserId(emotion.getUsers().getUserId());
        emotion.getEmotionPK().setArticleId(emotion.getArticle().getArticleId());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Emotion persistentEmotion = em.find(Emotion.class, emotion.getEmotionPK());
            Article articleOld = persistentEmotion.getArticle();
            Article articleNew = emotion.getArticle();
            Users usersOld = persistentEmotion.getUsers();
            Users usersNew = emotion.getUsers();
            if (articleNew != null) {
                articleNew = em.getReference(articleNew.getClass(), articleNew.getArticleId());
                emotion.setArticle(articleNew);
            }
            if (usersNew != null) {
                usersNew = em.getReference(usersNew.getClass(), usersNew.getUserId());
                emotion.setUsers(usersNew);
            }
            emotion = em.merge(emotion);
            if (articleOld != null && !articleOld.equals(articleNew)) {
                articleOld.getEmotionCollection().remove(emotion);
                articleOld = em.merge(articleOld);
            }
            if (articleNew != null && !articleNew.equals(articleOld)) {
                articleNew.getEmotionCollection().add(emotion);
                articleNew = em.merge(articleNew);
            }
            if (usersOld != null && !usersOld.equals(usersNew)) {
                usersOld.getEmotionCollection().remove(emotion);
                usersOld = em.merge(usersOld);
            }
            if (usersNew != null && !usersNew.equals(usersOld)) {
                usersNew.getEmotionCollection().add(emotion);
                usersNew = em.merge(usersNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                EmotionPK id = emotion.getEmotionPK();
                if (findEmotion(id) == null) {
                    throw new NonexistentEntityException("The emotion with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(EmotionPK id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Emotion emotion;
            try {
                emotion = em.getReference(Emotion.class, id);
                emotion.getEmotionPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The emotion with id " + id + " no longer exists.", enfe);
            }
            Article article = emotion.getArticle();
            if (article != null) {
                article.getEmotionCollection().remove(emotion);
                article = em.merge(article);
            }
            Users users = emotion.getUsers();
            if (users != null) {
                users.getEmotionCollection().remove(emotion);
                users = em.merge(users);
            }
            em.remove(emotion);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Emotion> findEmotionEntities() {
        return findEmotionEntities(true, -1, -1);
    }

    public List<Emotion> findEmotionEntities(int maxResults, int firstResult) {
        return findEmotionEntities(false, maxResults, firstResult);
    }

    private List<Emotion> findEmotionEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Emotion.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Emotion findEmotion(EmotionPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Emotion.class, id);
        } finally {
            em.close();
        }
    }

    public int getEmotionCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Emotion> rt = cq.from(Emotion.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
