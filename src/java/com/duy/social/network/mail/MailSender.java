package com.duy.social.network.mail;


import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class MailSender {

    public static final MailAccount DEFAULT_MAIL_ACCOUNT = new MailAccount("duytlse140829@fpt.edu.vn",
            ".Duy12345671");

    public static void send(MailAccount mailAccount, MailMessage email) {
        //create an instance of Properties Class   
        Properties props = new Properties();

        //  Specifies the IP address of your default mail server
        //   for e.g if you are using gmail server as an email sever
        // you will pass smtp.gmail.com as value of mail.smtp host.
        // As shown here in the code.
        // Change accordingly, if your email id is not a gmail id
        props.put("mail.smtp.host", "smtp.gmail.com");
        // below mentioned mail.smtp.port is optional 
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");

        // Pass Properties object(props) and Authenticator object
        // for authentication to Session instance

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(mailAccount.getEmail(), mailAccount.getPassword());
            }
        });

        try {

            //  Create an instance of MimeMessage,
            // it accept MIME types and headers
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(mailAccount.getEmail()));
            message.addRecipient(javax.mail.Message.RecipientType.TO, new InternetAddress(email.getDestMail()));
            message.setSubject(email.getSubject());
            message.setText(email.getContent());

            // Transport class is used to deliver the message to the recipients
            Transport.send(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}