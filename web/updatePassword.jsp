<%--
  Created by IntelliJ IDEA.
  User: duy
  Date: 9/25/2020
  Time: 6:05 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Update password</title>
    <jsp:include page="metadata.jsp"/>
</head>
<body>
<nav class="navbar navbar-light bg-light">
    <span class="navbar-brand h1">Update password</span>
    <jsp:include page="include/navigation.jsp"/>
</nav>

<div class="container">
    <form method="post" action="MainController">
        <input type="hidden" name="txtUserId" value="${sessionScope.user.userId}">
        <div class="form-group">
            <label>Old password
                <input type="password" name="txtPassword" maxlength="20" minlength="2" required
                       class="form-control">
            </label>
        </div>
        <div class="alert-danger"> ${requestScope.error.passwordError}</div>

        <div class="form-group">
            <label>New password
                <input type="password" name="txtNewPassword" maxlength="20" minlength="2" required
                       class="form-control">
            </label>
        </div>
        <div class="alert-danger">${requestScope.error.newPasswordError}</div>

        <div class="form-group">
            <label>Retype password
                <input type="password" name="txtRePassword" maxlength="20" minlength="2" required
                       class="form-control">
            </label>
        </div>
        <div class="alert-danger">${requestScope.error.rePasswordError}</div>

        <input type="submit" name="btnAction" value="Update password" class="btn btn-primary">
    </form>
</div>
</body>
</html>
