<%--
    Document   : article
    Created on : Sep 19, 2020, 2:01:47 PM
    Author     : duy
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Article Page</title>
    <link rel="stylesheet" href="css/style.css">
    <script src="js/main.js" type="text/javascript"></script>
    <jsp:include page="metadata.jsp"/>
</head>
<body>
<nav class="navbar navbar-light bg-light">
    <%--Welcom function--%>
    <span class="navbar-brand">Welcome, ${sessionScope.user.fullName}</span>
    <%--Search--%>
    <form action="MainController" method="post" class="form-inline">
        <input type="text" name="txtSearch" value="${param.txtSearch}" class="form-control mx-2" placeholder="Search">
        <input type="submit" name="btnAction" value="Search articles" class="btn btn-outline-success">
    </form>
    <jsp:include page="include/navigation.jsp"/>
</nav>
<div class="container align-content-center">
    <c:if test="${not empty requestScope.articles}">
        <c:forEach items="${requestScope.articles}" var="article" varStatus="counter">
            <div class="card my-2 w-50">
                <div class="card-header">
                    <span class="font-weight-bold">${article.userId.fullName}</span><br>
                    <span class="small">${article.date}</span>
                </div>
                <div class="card-body">
                    <span class="font-weight-bold">${article.title}</span> <br>
                    <span class="card-text">${article.description}</span>
                    <c:if test="${not empty article.imagePath}">
                        <div class="card-img">
                            <img class="mw-100" src="${pageContext.servletContext.contextPath}${article.imagePath}"
                                 alt="${article.title}">
                        </div>
                    </c:if>
                    <br>

                    <a class="btn btn-info"
                       href="MainController?btnAction=ViewArticleDetails&txtArticleId=${article.articleId}">
                        <i class="fa fa-info"></i>View details</a>
                    <c:if test="${sessionScope.user.userId == article.userId.userId || sessionScope.user.roleId.roleId == 1}">
                        <button class="btn btn-warning" onclick="showDeleteArticleConfirmationDialog('${article.articleId}')">
                            <i class="fa fa-remove"></i>Delete
                        </button>
                    </c:if>
                </div>
            </div>
        </c:forEach>
    </c:if>

    <c:if test="${empty requestScope.articles}">
        <p class="alert alert-info">There is no article</p>
    </c:if>

    <c:if test="${not empty requestScope.pageIndices}">
        <nav aria-label="Page navigation">
            <ul class="pagination">
                <c:forEach var="page" items="${requestScope.pageIndices}">
                    <li class="page-item">
                        <a class="page-link"
                           href="MainController?btnAction=View articles&page=${page}&txtSearch=${param.txtSearch}">${page}</a>
                    </li>
                </c:forEach>
            </ul>
        </nav>
    </c:if>
    <br>

</div>
</body>
</html>
