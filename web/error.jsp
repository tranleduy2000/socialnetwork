<%-- 
    Document   : error
    Created on : Sep 18, 2020, 11:22:53 PM
    Author     : duy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Error Page</title>
    <jsp:include page="metadata.jsp"/>
</head>
<body>
<div class="alert alert-danger">
    <h1>Error</h1>
    <h1>${param.message}</h1>
</div>
</body>
</html>
