<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: duy
  Date: 9/28/2020
  Time: 4:29 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Email verification</title>
    <jsp:include page="metadata.jsp"/>
</head>
<body>
<nav class="navbar navbar-light bg-light">
    <span class="navbar-brand">Email verification</span>
</nav>
<div class="container align-content-center">
    <p>A verification code was sent to <b>${sessionScope.user.userId}</b></p>
    <form method="post" action="MainController">
        <div class="form-group">
            <label for="txtEnteredCode">Enter 6-digits code:</label>
            <input class="form-control" id="txtEnteredCode"
                   maxlength="6" minlength="6" name="txtEnteredCode" required type="number">
        </div>
        <c:if test="${not empty requestScope.error}">
            <div class="alert alert-danger">${requestScope.error}</div>
        </c:if>
        <input type="submit" name="btnAction" value="Verify Code" class="btn btn-primary">
    </form>
    <a href="MainController?btnAction=Send Verification Code&txtUserId=${sessionScope.user.userId}">Resend code</a>
</div>
</body>
</html>
