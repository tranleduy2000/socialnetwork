/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duy.social.network.article;

import com.duy.social.network.jpa.entity.Article;
import com.duy.social.network.jpa.entity.Users;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author duy
 */
public class DeleteArticleController extends HttpServlet {

    private static final String ERROR_PAGE = "error.jsp";
    private static final String ARTICLE = ArticleController.class.getSimpleName();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        String url = ERROR_PAGE;
        try {
            int articleId = Integer.parseInt(request.getParameter("txtArticleId"));
            HttpSession session = request.getSession(true);
            Users user = (Users) session.getAttribute("user");
            if (user != null) {
                ArticleBLO articleBLO = new ArticleBLO();
                Article article = articleBLO.get(articleId);
                int adminRole = 1;
                if (article.getUserId().getUserId().equals(user.getUserId())
                        || user.getRoleId().getRoleId().equals(adminRole)) {
                    boolean success = articleBLO.deactive(articleId);
                    if (success) {
                        url = ARTICLE;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log(e.getMessage());
        }
        response.sendRedirect(url);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
