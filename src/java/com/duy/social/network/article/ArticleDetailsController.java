/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duy.social.network.article;

import com.duy.social.network.comment.CommentBLO;
import com.duy.social.network.emotion.EmotionBLO;
import com.duy.social.network.jpa.entity.Article;
import com.duy.social.network.jpa.entity.Comment;
import com.duy.social.network.jpa.entity.Emotion;
import com.duy.social.network.jpa.entity.Users;
import com.duy.social.network.utils.Constants;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * @author duy
 */
public class ArticleDetailsController extends HttpServlet {


    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String url = Constants.ERROR_PAGE;

        try {
            HttpSession session = request.getSession(true);
            Users user = (Users) session.getAttribute("user");

            if (user != null) {
                String articleId = request.getParameter("txtArticleId");

                ArticleBLO articleBLO = new ArticleBLO();
                Article article = articleBLO.get(Integer.parseInt(articleId));
                request.setAttribute("article", article);

                EmotionBLO emotionBLO = new EmotionBLO();
                Emotion emotion = emotionBLO.getEmotion(user, article);

                CommentBLO commentBLO = new CommentBLO();
                List<Comment> comments = commentBLO.getActiveCommentsOfArticleSortByDate(article);
                request.setAttribute("comments", comments);
                request.setAttribute("totalLike", emotionBLO.getNumberOfEmotions(article, EmotionBLO.LIKE));
                request.setAttribute("totalDislike", emotionBLO.getNumberOfEmotions(article, EmotionBLO.DISLIKE));
                request.setAttribute("emotion", emotion); // current user emotion

                url = Constants.ARTICLE_DETAILS_PAGE;
            }

        } catch (Exception e) {
            log(e.getMessage(), e);
        }
        request.getRequestDispatcher(url).forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
