/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duy.social.network.article;

import com.duy.social.network.jpa.entity.Article;
import com.duy.social.network.utils.Constants;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author duy
 */
public class ArticleController extends HttpServlet {


    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String url = Constants.ERROR_PAGE;
        try {
            String searchValue = request.getParameter("txtSearch");
            if (searchValue == null) {
                searchValue = "";
            }
            String pageIndexStr = request.getParameter("page");
            if (pageIndexStr == null || pageIndexStr.isEmpty()) {
                pageIndexStr = "0";
            }
            int pageIndex = Integer.parseInt(pageIndexStr);

            ArticleBLO articleBLO = new ArticleBLO();
            List<Article> articles = articleBLO.getActiveArticlesSortByDateFilterByKeyword(pageIndex, searchValue);
            request.setAttribute("articles", articles);
            request.setAttribute("currentPage", pageIndex);
            request.setAttribute("pageIndices", generatePageIndices(
                    articleBLO.getNumberOfActiveArticlesFilterByKeyword(searchValue),
                    ArticleBLO.NUMBER_OF_ARTICLES_PER_PAGE, pageIndex));

            url = Constants.ARTICLE_PAGE;
        } catch (Exception e) {
            e.printStackTrace();
            log(e.getMessage());
        }
        request.getRequestDispatcher(url).forward(request, response);
    }

    @SuppressWarnings("SameParameterValue")
    private List<Integer> generatePageIndices(int articleCount, int numberOfArticlesPerPage, int currentPageIndex) {
        // Ex: 41 / 20 = 3 pages
        int maxPageIndex = (int) Math.ceil(articleCount * 1.0 / numberOfArticlesPerPage) - 1;

        if (currentPageIndex == 0) {
            if (currentPageIndex + 1 <= maxPageIndex) {
                return Arrays.asList(0, currentPageIndex + 1);
            }
        } else if (currentPageIndex == maxPageIndex) {
            if (currentPageIndex - 1 >= 0) {
                return Arrays.asList(currentPageIndex - 1, currentPageIndex);
            }
        } else if (maxPageIndex >= 2) {
            return Arrays.asList(currentPageIndex - 1, currentPageIndex, currentPageIndex + 1);
        }
        return new ArrayList<>(); // there is one page
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
