<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
    Document   : userDetails
    Created on : Sep 25, 2020, 5:40:17 PM
    Author     : duy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Profile Page</title>
    <jsp:include page="metadata.jsp"/>
</head>
<body>
<nav class="navbar navbar-light bg-light">
    <span class="navbar-brand">Profile</span>
    <jsp:include page="include/navigation.jsp"/>
</nav>
<div class="container">
    <c:if test="${not empty requestScope.message}">
        <div class="alert alert-success">
                ${requestScope.message}
        </div>
    </c:if>
    <c:set var="user" value="${sessionScope.user}"/>
    <form method="post" action="MainController">
        <div class="form-group">
            <label>Full name
                <input name="txtFullName" maxlength="50" minlength="2" required value="${user.fullName}"
                       class="form-control">
            </label>
        </div>
        <div class="alert-danger">${requestScope.error.fullNameError}</div>
        <div class="form-group">
            <label>Email
                <input name="txtUserId" maxlength="50" type="email" class="form-control"
                       readonly
                       value="${user.userId}">
            </label>
        </div>
        <div class="alert-danger">${requestScope.error.userIdError} </div>
        <input type="submit" name="btnAction" value="Update Account" class="btn btn-primary">
    </form>


    <a class="btn btn-outline-primary my-2" href="updatePassword.jsp">Update password</a>
</div>
</body>
</html>
