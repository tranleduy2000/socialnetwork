package com.duy.social.network.article;

import com.duy.social.network.jpa.controller.ArticleJpaController;
import com.duy.social.network.jpa.entity.Article;
import com.duy.social.network.utils.DbUtils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

public class ArticleBLO {
    public static final boolean ACTIVE_STATUS = true;

    public static final int NUMBER_OF_ARTICLES_PER_PAGE = 20;

    private final EntityManagerFactory entityManagerFactory = DbUtils.getInstance();

    public List<Article> getActiveArticlesSortByDateFilterByKeyword(int page, String keyword) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            return entityManager
                    .createQuery("SELECT a " +
                            "FROM Article a " +
                            "WHERE a.status = TRUE AND (a.title LIKE :keyword OR a.description LIKE :keyword) " +
                            "ORDER BY a.date DESC", Article.class)
                    .setParameter("keyword", "%" + keyword + "%")
                    .setFirstResult(NUMBER_OF_ARTICLES_PER_PAGE * page)
                    .setMaxResults(NUMBER_OF_ARTICLES_PER_PAGE)
                    .getResultList();
        } finally {
            entityManager.close();
        }
    }

    public int getNumberOfActiveArticlesFilterByKeyword(String keyword) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            return entityManager
                    .createQuery("SELECT COUNT(a) " +
                            "FROM Article a " +
                            "WHERE a.status = TRUE AND (a.title LIKE :keyword OR a.description LIKE :keyword)", Long.class)
                    .setParameter("keyword", "%" + keyword + "%")
                    .getSingleResult().intValue();
        } finally {
            entityManager.close();
        }
    }

    public boolean insert(Article article) {
        try {
            ArticleJpaController articleJpaController = new ArticleJpaController(entityManagerFactory);
            articleJpaController.create(article);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public Article get(int articleId) {
        ArticleJpaController articleJpaController = new ArticleJpaController(entityManagerFactory);
        return articleJpaController.findArticle(articleId);
    }

    public boolean deactive(int articleId) {
        try {
            ArticleJpaController articleJpaController = new ArticleJpaController(entityManagerFactory);
            Article article = articleJpaController.findArticle(articleId);
            article.setStatus(false);
            articleJpaController.edit(article);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
