package com.duy.social.network.user.blo;

import com.duy.social.network.jpa.controller.UsersJpaController;
import com.duy.social.network.jpa.entity.Role;
import com.duy.social.network.jpa.entity.Users;
import com.duy.social.network.utils.DbUtils;
import org.apache.commons.codec.digest.DigestUtils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.nio.charset.StandardCharsets;

public class UserBLO {

    private static final Integer USER_ROLE = 0;
    public static final Integer ADMIN_ROLE = 1;

    public static final String STATUS_NEW = "New";
    public static final String STATUS_ACTIVE = "Active";

    private final EntityManagerFactory entityManagerFactory = DbUtils.getInstance();

    public Users checkLogin(String userId, String password) {
        String hashedPassword = DigestUtils.sha256Hex(password.getBytes(StandardCharsets.UTF_8));
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        String jpql = "SELECT u FROM Users u WHERE u.userId = :userId AND u.password = :password";
        TypedQuery<Users> query = entityManager.createQuery(jpql, Users.class);
        query.setParameter("userId", userId);
        query.setParameter("password", hashedPassword);
        try {
            Users user = query.getSingleResult();
            user.setPassword("");
            return user;
        } catch (NoResultException e) {
            return null;
        } finally {
            entityManager.close();
        }
    }

    public Users get(String userId) {
        UsersJpaController controller = new UsersJpaController(entityManagerFactory);
        try {
            Users user = controller.findUsers(userId);
            user.setPassword(""); // remove password
            return user;
        } catch (Exception e) {
            return null;
        }
    }

    public boolean insert(String userId, String password, String fullName) {
        try {
            String hashedPassword = DigestUtils.sha256Hex(password);
            Users user = new Users(userId, fullName, STATUS_NEW);
            user.setPassword(hashedPassword);
            user.setRoleId(new Role(USER_ROLE));

            UsersJpaController usersJpaController = new UsersJpaController(entityManagerFactory);
            usersJpaController.create(user);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean update(String userId, String fullName) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            int result = entityManager.createQuery(
                    "UPDATE Users SET fullName = :fullName WHERE userId = :userId")
                    .setParameter("userId", userId)
                    .setParameter("fullName", fullName).executeUpdate();
            entityManager.getTransaction().commit();
            return result > 0;
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            return false;
        } finally {
            entityManager.close();
        }
    }

    public boolean updatePassword(String userId, String oldPassword, String newPassword) {
        String hashedOldPassword = DigestUtils.sha256Hex(oldPassword);
        String hashedNewPassword = DigestUtils.sha256Hex(newPassword);

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            int result = entityManager.createQuery(
                    "UPDATE Users SET password = :hashedNewPassword " +
                            "WHERE userId = :userId AND password = :hashedOldPassword")
                    .setParameter("userId", userId)
                    .setParameter("hashedOldPassword", hashedOldPassword)
                    .setParameter("hashedNewPassword", hashedNewPassword).executeUpdate();
            entityManager.getTransaction().commit();
            return result > 0;
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            return false;
        } finally {
            entityManager.close();
        }
    }

    public boolean updateVerificationCode(String userId, String verificationCode) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            int result = entityManager.createQuery(
                    "UPDATE Users SET verificationCode = :code WHERE userId = :userId")
                    .setParameter("code", verificationCode)
                    .setParameter("userId", userId)
                    .executeUpdate();
            entityManager.getTransaction().commit();
            return result > 0;
        } catch (Exception e) {
            e.printStackTrace();
            entityManager.getTransaction().rollback();
            return false;
        } finally {
            entityManager.close();
        }
    }

    public boolean checkVerificationCode(String userId, String enteredCode) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            int result = entityManager.createQuery(
                    "UPDATE Users SET status = 'Active' WHERE userId = :userId AND verificationCode = :enteredCode")
                    .setParameter("enteredCode", enteredCode)
                    .setParameter("userId", userId)
                    .executeUpdate();
            entityManager.getTransaction().commit();
            return result > 0;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            entityManager.close();
        }
    }
}
