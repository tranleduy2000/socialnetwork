/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duy.social.network.user;

import com.duy.social.network.jpa.entity.Users;
import com.duy.social.network.mail.MailMessage;
import com.duy.social.network.mail.MailSender;
import com.duy.social.network.user.blo.UserBLO;
import com.duy.social.network.utils.Constants;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Random;

/**
 * @author duy
 */
public class SendVerificationCodeController extends HttpServlet {


    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        String url = Constants.ERROR_PAGE;
        try {
            String userId = request.getParameter("txtUserId");
            final UserBLO userBLO = new UserBLO();
            Users user = userBLO.get(userId);
            if (user == null) {
                url = Constants.LOGIN_PAGE;

            } else if (user.getStatus().equalsIgnoreCase(UserBLO.STATUS_ACTIVE)) {
                url = Constants.ARTICLE;

            } else {
                final Random random = new Random();
                String code = generateCode(random, 6);

                if (userBLO.updateVerificationCode(user.getUserId(), code)) {
                    user = userBLO.get(user.getUserId());
                    HttpSession session = request.getSession(true);
                    session.setAttribute("user", user); // update user info

                    String subject = "Verification code";
                    String content = String.format("Hello %s\nVerification code is: %s", user.getFullName(), code);
                    MailSender.send(MailSender.DEFAULT_MAIL_ACCOUNT,
                            new MailMessage(user.getUserId(), subject, content));
                    url = Constants.ENTER_VERIFICATION_CODE_PAGE;
                }
            }
        } catch (Exception e) {
            log(e.getMessage(), e);
        }

        response.sendRedirect(url); // do not allow user repeats this action
    }

    @SuppressWarnings("SameParameterValue")
    private String generateCode(Random random, int length) {
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < length; i++) {
            s.append(random.nextInt(10)); // 0-9
        }
        return s.toString();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
