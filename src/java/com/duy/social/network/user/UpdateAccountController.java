/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duy.social.network.user;

import com.duy.social.network.jpa.entity.Users;
import com.duy.social.network.user.blo.UserBLO;
import com.duy.social.network.user.dto.UserErrorDTO;
import com.duy.social.network.utils.Constants;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author duy
 */
public class UpdateAccountController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String url = Constants.ERROR_PAGE;
        try {
            HttpSession session = request.getSession(true);
            String userId = request.getParameter("txtUserId");
            String fullName = request.getParameter("txtFullName");

            Users user = (Users) session.getAttribute("user");
            UserErrorDTO userErrorDTO = new UserErrorDTO();
            if (user.getUserId().equals(userId)
                    || user.getRoleId().getRoleId().equals(UserBLO.ADMIN_ROLE)) { // avoid illegal access

                UserBLO userBLO = new UserBLO();
                boolean error = false;
                if (fullName.length() < 2 || fullName.length() > 50) {
                    error = true;
                    userErrorDTO.setFullNameError("Length of full name must be from 2 to 50");
                }

                if (error) {
                    request.setAttribute("error", userErrorDTO);
                    url = Constants.PROFILE_PAGE;

                } else if (userBLO.update(userId, fullName)) {
                    session.setAttribute("user", userBLO.get(userId));
                    request.setAttribute("message", "Update success");
                    url = Constants.PROFILE_PAGE;

                } else {
                    log("Cannot update user info");
                }
            }
        } catch (Exception e) {
            log(e.getMessage(), e);
        }
        request.getRequestDispatcher(url).forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
