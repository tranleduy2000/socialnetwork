<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
<head>
    <title>Login Page</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <jsp:include page="metadata.jsp"/>
</head>
<body>
<nav class="navbar navbar-link bg-light">
    <span class="navbar-brand">Login</span>
</nav>
<div class="container">

    <form method="post" action="MainController">
        <div class="form-group">

            <label for="txtUserId"><i class="fa fa-envelope"></i> Email: </label><input id="txtUserId" name="txtUserId"
                                                                                        maxlength="50" required
                                                                                        class="form-control">
        </div>
        <div class="form-group">
            <label for="txtPassword"><i class="fa fa-key"></i> Password: </label><input id="txtPassword"
                                                                                       name="txtPassword"
                                                                                       maxlength="12"
                                                                                       type="password" required
                                                                                       class="form-control">
        </div>
        <c:if test="${not empty param.error}">
            <div class="alert alert-danger">${param.error}</div>
        </c:if>
        <input type="submit" name="btnAction" value="Login" class="btn btn-primary">
        <input type="reset" name="btnReset" class="btn btn-warning">
    </form>
    Not registered? <a href="createAccount.jsp">Create new account</a>
</div>
</body>
</html>
