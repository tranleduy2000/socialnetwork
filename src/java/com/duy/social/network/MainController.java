/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duy.social.network;

import com.duy.social.network.utils.Constants;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author duy
 */
public class MainController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String url = Constants.ERROR_PAGE;
        try {
            String action = request.getParameter("btnAction");

            if (action != null) {
                if (action.equalsIgnoreCase("Create Account")) {
                    url = Constants.CREATE_ACCOUNT;

                } else if (action.equalsIgnoreCase("Login")) {
                    url = Constants.LOGIN;

                } else if (action.equalsIgnoreCase("Logout")) {
                    url = Constants.LOGOUT;

                } else if (action.equalsIgnoreCase("Send Verification Code")) {
                    url = Constants.SEND_VERIFICATION_CODE;

                } else if (action.equalsIgnoreCase("Verify Code")) {
                    url = Constants.VERIFY_CODE;

                } else if (action.equalsIgnoreCase("Update Account")) {
                    url = Constants.UPDATE_ACCOUNT;

                } else if (action.equalsIgnoreCase("Update password")) {
                    url = Constants.UPDATE_PASSWORD;

                } else if (action.equalsIgnoreCase("View articles")) {
                    url = Constants.VIEW_ARTICLES;

                } else if (action.equalsIgnoreCase("Post article")) {
                    url = Constants.CREATE_ARTICLE;

                } else if (action.equalsIgnoreCase("Search articles")) {
                    url = Constants.VIEW_ARTICLES;

                } else if (action.equalsIgnoreCase("DeleteArticle")) {
                    url = Constants.DELETE_ARTICLE;

                } else if (action.equalsIgnoreCase("ViewArticleDetails")) {
                    url = Constants.ARTICLE_DETAILS;

                } else if (action.equalsIgnoreCase("PostComment")) {
                    url = Constants.CREATE_COMMENT;

                } else if (action.equalsIgnoreCase("DeleteComment")) {
                    url = Constants.DELETE_COMMENT;

                } else if (action.equalsIgnoreCase("CreateEmotion")) {
                    url = Constants.CREATE_EMOTION;

                } else if (action.equalsIgnoreCase("ViewNotification")) {
                    url = Constants.VIEW_NOTIFICATION;

                } else if (action.equalsIgnoreCase("DeleteNotification")) {
                    url = Constants.DELETE_NOTIFICATION;

                } else {
                    log("Undefined action");
                }
            }

            log("action: " + action);
            log("url: " + url);
        } catch (Exception e) {
            log(e.getMessage(), e);
        }

        request.getRequestDispatcher(url).forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
