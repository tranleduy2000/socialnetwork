/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duy.social.network;

import com.duy.social.network.jpa.entity.Users;
import com.duy.social.network.providers.ImageProvider;
import com.duy.social.network.user.VerifyCodeController;
import com.duy.social.network.user.blo.UserBLO;
import com.duy.social.network.utils.Constants;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author duy
 */
public class MainFilter implements Filter {

    private static final boolean debug = true;

    private final Set<String> unrestrictedResources = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
    private final Set<String> unverifiedUserResources = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
    private final Set<String> verifiedUserResources = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
    private final Set<String> adminResource = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);


    // The filter configuration object we are associated with.  If
    // this value is null, this filter instance is not currently
    // configured. 
    private FilterConfig filterConfig = null;

    public MainFilter() {
        unrestrictedResources.add(""); // root of site
        unrestrictedResources.add(MainController.class.getSimpleName());
        unrestrictedResources.add(Constants.CREATE_ACCOUNT_PAGE);
        unrestrictedResources.add(Constants.CREATE_ACCOUNT);
        unrestrictedResources.add(Constants.LOGIN_PAGE);
        unrestrictedResources.add(Constants.LOGIN);
        unrestrictedResources.add(Constants.ERROR_PAGE);
        unrestrictedResources.add(ImageProvider.class.getSimpleName());

        unverifiedUserResources.add(Constants.ENTER_VERIFICATION_CODE_PAGE);
        unverifiedUserResources.add(Constants.SEND_VERIFICATION_CODE);
        unverifiedUserResources.add(VerifyCodeController.class.getSimpleName());
        unverifiedUserResources.add(Constants.LOGOUT);

        verifiedUserResources.add(Constants.LOGOUT);
        verifiedUserResources.add("userDetails.jsp");
        verifiedUserResources.add(Constants.UPDATE_ACCOUNT);
        verifiedUserResources.add(Constants.ARTICLE_PAGE);
        verifiedUserResources.add("updatePassword.jsp");
        verifiedUserResources.add(Constants.UPDATE_PASSWORD);
        verifiedUserResources.add(Constants.ARTICLE);
        verifiedUserResources.add("postArticle.jsp");
        verifiedUserResources.add(Constants.CREATE_ARTICLE);
        verifiedUserResources.add(Constants.ARTICLE_DETAILS_PAGE);
        verifiedUserResources.add(Constants.ARTICLE_DETAILS);
        verifiedUserResources.add(Constants.CREATE_COMMENT);
        verifiedUserResources.add(Constants.DELETE_COMMENT);
        verifiedUserResources.add(Constants.DELETE_ARTICLE);
        verifiedUserResources.add(Constants.CREATE_EMOTION);
        verifiedUserResources.add(Constants.NOTIFICATION_PAGE);
        verifiedUserResources.add(Constants.NOTIFICATION);
        verifiedUserResources.add(Constants.DELETE_NOTIFICATION);

        adminResource.add(Constants.LOGOUT);
        adminResource.add("userDetails.jsp");
        adminResource.add(Constants.UPDATE_ACCOUNT);
        adminResource.add(Constants.ARTICLE_PAGE);
        adminResource.add(Constants.UPDATE_PASSWORD);
        adminResource.add("updatePassword.jsp");
        adminResource.add(Constants.ARTICLE);
        adminResource.add(Constants.ARTICLE_DETAILS_PAGE);
        adminResource.add(Constants.ARTICLE_DETAILS);
        adminResource.add(Constants.DELETE_COMMENT);
        adminResource.add(Constants.DELETE_ARTICLE);
    }

    /**
     * @param request  The servlet request we are processing
     * @param response The servlet response we are creating
     * @param chain    The filter chain we are processing
     * @throws IOException      if an input/output error occurs
     * @throws ServletException if a servlet error occurs
     */
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain)
            throws IOException, ServletException {

        if (debug) {
            log("MainFilter:doFilter()");
        }

        try {
            final HttpServletRequest servletRequest = (HttpServletRequest) request;
            final String requestURI = servletRequest.getRequestURI();
            String resourceFile = "";
            if (requestURI.contains("/")) {
                resourceFile = requestURI.substring(requestURI.lastIndexOf("/") + 1);
            }
            log("uri: " + requestURI);
            log("resourceFile: " + resourceFile);

            if (unrestrictedResources.contains(resourceFile)
                    || resourceFile.matches("(.*?)\\.(jpg|jpep|png|css|js)")) {
                log("access granted");
                chain.doFilter(request, response);
                return;
            }

            // check if user did login
            HttpSession session = servletRequest.getSession(true);
            HttpServletResponse servletResponse = (HttpServletResponse) response;
            if (session.getAttribute("user") == null) {
                log("Unauthenticated user. Access denied");
                servletResponse.sendRedirect(Constants.LOGIN_PAGE);
                return;
            }

            Users user = (Users) session.getAttribute("user");

            // User did not active email
            if (user.getStatus().equalsIgnoreCase(UserBLO.STATUS_NEW)) {
                if (unverifiedUserResources.contains(resourceFile)) {
                    chain.doFilter(request, response);
                    return;
                }
                log("User did not verify email. Access denied");
                if (user.getVerificationCode() == null || user.getVerificationCode().isEmpty()) {
                    servletResponse.sendRedirect(Constants.SEND_VERIFICATION_CODE);
                } else {
                    servletResponse.sendRedirect(Constants.ENTER_VERIFICATION_CODE_PAGE);
                }
                return;
            }

            if (user.getRoleId().getRoleId().equals(0)) { // User
                if (verifiedUserResources.contains(resourceFile)) {
                    log("access granted");
                    chain.doFilter(request, response);
                    return;
                }
            } else if (user.getRoleId().getRoleId().equals(1)) { // Admin
                if (adminResource.contains(resourceFile)) {
                    log("access granted");
                    chain.doFilter(request, response);
                    return;
                }
            }

        } catch (Exception e) {
            log(e.getMessage());
        }
        log("Access denied");

        // Invalid request, direct to error page
        request.setAttribute("message", "Invalid request");
        request.getRequestDispatcher(Constants.ERROR_PAGE).forward(request, response);
    }

    /**
     * Return the filter configuration object for this filter.
     */
    public FilterConfig getFilterConfig() {
        return (this.filterConfig);
    }

    /**
     * Set the filter configuration object for this filter.
     *
     * @param filterConfig The filter configuration object
     */
    public void setFilterConfig(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }

    /**
     * Destroy method for this filter
     */
    public void destroy() {
    }

    /**
     * Init method for this filter
     */
    public void init(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
        if (filterConfig != null) {
            if (debug) {
                log("MainFilter:Initializing filter");
            }
        }
    }

    /**
     * Return a String representation of this object.
     */
    @Override
    public String toString() {
        if (filterConfig == null) {
            return ("MainFilter()");
        }
        StringBuffer sb = new StringBuffer("MainFilter(");
        sb.append(filterConfig);
        sb.append(")");
        return (sb.toString());
    }

    public void log(String msg) {
        filterConfig.getServletContext().log(msg);
    }

}
