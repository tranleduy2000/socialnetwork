package com.duy.social.network.notification;

import com.duy.social.network.jpa.controller.NotificationJpaController;
import com.duy.social.network.jpa.entity.Article;
import com.duy.social.network.jpa.entity.Notification;
import com.duy.social.network.jpa.entity.Users;
import com.duy.social.network.utils.DbUtils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.Date;
import java.util.List;

public class NotificationBLO {
    public static final int LIKE = 1;
    public static final int DISLIKE = 2;
    public static final int COMMENT = 3;

    private static final int ADMIN_ROLE = 1;

    private final EntityManagerFactory entityManagerFactory
            = DbUtils.getInstance();

    public List<Notification> getActiveNotifications(Users user) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            return entityManager.createQuery("SELECT n FROM Notification n " +
                            "WHERE n.articleId.userId = :userId AND n.userId <> :userId " +
                            "AND n.active = true AND n.articleId.status = true " +
                            "ORDER BY n.date DESC",
                    Notification.class)
                    .setParameter("userId", user)
                    .getResultList();
        } finally {
            entityManager.close();
        }
    }

    public boolean create(Users user, Article article, int activityType, Integer objectId) {
        try {
            NotificationJpaController notificationJpaController = new NotificationJpaController(entityManagerFactory);
            long date = System.currentTimeMillis();
            Notification notification = new Notification(0, activityType, new Date(date), true);
            notification.setUserId(user);
            notification.setArticleId(article);
            notification.setObjectId(objectId);
            notificationJpaController.create(notification);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean deactive(Users user, int notificationId) {
        try {
            NotificationJpaController notificationJpaController = new NotificationJpaController(entityManagerFactory);
            Notification notification = notificationJpaController.findNotification(notificationId);

            // only admin or notification's user can delete this notification
            if (user.getUserId().equals(notification.getArticleId().getUserId().getUserId())
                    || user.getRoleId().getRoleId().equals(ADMIN_ROLE)) {
                notification.setActive(false);
                notificationJpaController.edit(notification);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
