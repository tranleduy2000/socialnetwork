/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duy.social.network.comment;

import com.duy.social.network.article.ArticleBLO;
import com.duy.social.network.jpa.entity.Article;
import com.duy.social.network.jpa.entity.Comment;
import com.duy.social.network.jpa.entity.Users;
import com.duy.social.network.notification.NotificationBLO;
import com.duy.social.network.utils.Constants;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author duy
 */
public class CreateCommentController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        String urlRewriting = Constants.ERROR_PAGE;
        try {
            HttpSession session = request.getSession(true);
            Users user = (Users) session.getAttribute("user");
            if (user == null) {
                urlRewriting = Constants.LOGIN_PAGE;

            } else {
                int articleId = Integer.parseInt(request.getParameter("txtArticleId"));
                String commentContent = request.getParameter("txtCommentContent");

                ArticleBLO articleBLO = new ArticleBLO();
                Article article = articleBLO.get(articleId);

                CommentBLO commentBLO = new CommentBLO();
                Comment comment = commentBLO.insert(user, article, commentContent);
                boolean inserted = comment != null;
                if (inserted) {

                    // do not need transaction
                    NotificationBLO notificationBLO = new NotificationBLO();
                    if (notificationBLO.create(user, article, NotificationBLO.COMMENT, comment.getCommentId())) {
                        urlRewriting = String.format("MainController?btnAction=ViewArticleDetails&txtArticleId=%d", articleId);
                    }
                }
            }
        } catch (Exception e) {
            log(e.getMessage(), e);
        }
        response.sendRedirect(urlRewriting); // do not allow user repeats this action
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
