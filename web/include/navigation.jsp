<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: duy
  Date: 9/25/2020
  Time: 4:22 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<ul class="nav nav-pills">
    <li class="nav-item">
        <a class="nav-link" href="MainController?btnAction=View articles"><i class="fa fa-home"></i>Home</a>
    </li>
    <c:if test="${sessionScope.user.roleId.roleId == 0}">
        <li class="nav-item">
            <a class="nav-link" href="postArticle.jsp"><i class="fa fa-plus-circle"></i>Post article</a>
        </li>
    </c:if>
    <li class="nav-item"><a class="nav-link" href="MainController?btnAction=ViewNotification">
        <i class="fa fa-bell"></i>Notification</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="MainController?btnAction=Logout"><i class="fa fa-sign-out"></i>Logout</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="userDetails.jsp"><i class="fa fa-user"></i>Profile</a>
    </li>
</ul>

