<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
    Document   : notification
    Created on : Sep 21, 2020, 10:55:28 PM
    Author     : duy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Notification Page</title>
    <jsp:include page="metadata.jsp"/>
</head>
<body>
<nav class="navbar navbar-light bg-light">
    <span class="navbar-brand">Notification</span>
    <jsp:include page="include/navigation.jsp"/>
</nav>
<div class="container">
    <c:if test="${not empty requestScope.notifications}">
        <c:forEach items="${requestScope.notifications}" var="notification">
            <div class="card my-2">
                <div class="card-body">
                    <span class="card-title">${notification.message}</span>
                    <br>
                    <span class="card-text small">${notification.date}</span><br>
                    <a class="btn btn-primary"
                       href="MainController?btnAction=ViewArticleDetails&txtArticleId=${notification.article.articleId}">View
                        details</a>
                    <a class="btn btn-warning"
                       href="MainController?btnAction=DeleteNotification&txtNotificationId=${notification.notificationId}">Delete</a>
                </div>
            </div>
        </c:forEach>
    </c:if>
    <c:if test="${empty requestScope.notifications}">
        <p class="alert alert-info">You do not have any notification</p>
    </c:if>
</div>
</body>
</html>
