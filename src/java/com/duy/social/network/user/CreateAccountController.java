package com.duy.social.network.user;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.duy.social.network.user.blo.UserBLO;
import com.duy.social.network.user.dto.UserErrorDTO;
import com.duy.social.network.utils.Constants;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author duy
 */
public class CreateAccountController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String url = Constants.CREATE_ACCOUNT_PAGE;
        try {
            final String userId = request.getParameter("txtUserId");
            final String password = request.getParameter("txtPassword");
            final String rePassword = request.getParameter("txtRePassword");
            final String fullName = request.getParameter("txtFullName");

            boolean error = false;
            UserErrorDTO userErrorDTO = new UserErrorDTO();
            if (userId.length() < 2 || userId.length() > 50) {
                error = true;
                userErrorDTO.setUserIdError("Length of email must be from 2 to 50");

            } else if (!userId.matches("[A-Za-z0-9.]+@[A-Za-z0-9.]+")) { // simply email pattern
                error = true;
                userErrorDTO.setUserIdError("Invalid email format");
            }

            if (password.length() < 2 || password.length() > 20) {
                error = true;
                userErrorDTO.setPasswordError("Password length must be from 2 to 12 characters");
            }

            if (!password.equals(rePassword)) {
                error = true;
                userErrorDTO.setRePasswordError("Passwords do not match");
            }

            if (fullName.length() < 2 || fullName.length() > 50) {
                error = true;
                userErrorDTO.setFullNameError("Length of full name must be from 2 to 50");
            }

            if (error) {
                request.setAttribute("error", userErrorDTO);

            } else {
                UserBLO userBLO = new UserBLO();
                if (userBLO.insert(userId, password, fullName)) {
                    url = Constants.SEND_VERIFICATION_CODE + "?txtUserId=" + userId;

                } else {
                    userErrorDTO.setUserIdError("Account is exist");
                    request.setAttribute("error", userErrorDTO);
                }
            }

        } catch (Exception e) {
            log(e.getMessage(), e);
        }
        request.getRequestDispatcher(url).forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
