function showDeleteArticleConfirmationDialog(articleId) {
    if (confirm("Delete this article?")) {
        window.location.href = "MainController?" +
            "btnAction=DeleteArticle" +
            "&txtArticleId=" + articleId
    }
}


function showDeleteCommentConfirmationDialog(articleId, commentId) {
    if (confirm("Delete this comment?")) {
        window.location.href = "MainController?" +
            "btnAction=DeleteComment" +
            "&txtArticleId=" + articleId +
            "&txtCommentId=" + commentId;
    }
}
