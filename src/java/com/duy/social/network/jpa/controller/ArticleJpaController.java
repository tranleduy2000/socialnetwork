/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duy.social.network.jpa.controller;

import com.duy.social.network.jpa.controller.exceptions.IllegalOrphanException;
import com.duy.social.network.jpa.controller.exceptions.NonexistentEntityException;
import com.duy.social.network.jpa.entity.Article;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.duy.social.network.jpa.entity.Users;
import com.duy.social.network.jpa.entity.Comment;
import java.util.ArrayList;
import java.util.Collection;
import com.duy.social.network.jpa.entity.Emotion;
import com.duy.social.network.jpa.entity.Notification;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author duy
 */
public class ArticleJpaController implements Serializable {

    public ArticleJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Article article) {
        if (article.getCommentCollection() == null) {
            article.setCommentCollection(new ArrayList<Comment>());
        }
        if (article.getEmotionCollection() == null) {
            article.setEmotionCollection(new ArrayList<Emotion>());
        }
        if (article.getNotificationCollection() == null) {
            article.setNotificationCollection(new ArrayList<Notification>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Users userId = article.getUserId();
            if (userId != null) {
                userId = em.getReference(userId.getClass(), userId.getUserId());
                article.setUserId(userId);
            }
            Collection<Comment> attachedCommentCollection = new ArrayList<Comment>();
            for (Comment commentCollectionCommentToAttach : article.getCommentCollection()) {
                commentCollectionCommentToAttach = em.getReference(commentCollectionCommentToAttach.getClass(), commentCollectionCommentToAttach.getCommentId());
                attachedCommentCollection.add(commentCollectionCommentToAttach);
            }
            article.setCommentCollection(attachedCommentCollection);
            Collection<Emotion> attachedEmotionCollection = new ArrayList<Emotion>();
            for (Emotion emotionCollectionEmotionToAttach : article.getEmotionCollection()) {
                emotionCollectionEmotionToAttach = em.getReference(emotionCollectionEmotionToAttach.getClass(), emotionCollectionEmotionToAttach.getEmotionPK());
                attachedEmotionCollection.add(emotionCollectionEmotionToAttach);
            }
            article.setEmotionCollection(attachedEmotionCollection);
            Collection<Notification> attachedNotificationCollection = new ArrayList<Notification>();
            for (Notification notificationCollectionNotificationToAttach : article.getNotificationCollection()) {
                notificationCollectionNotificationToAttach = em.getReference(notificationCollectionNotificationToAttach.getClass(), notificationCollectionNotificationToAttach.getNotificationId());
                attachedNotificationCollection.add(notificationCollectionNotificationToAttach);
            }
            article.setNotificationCollection(attachedNotificationCollection);
            em.persist(article);
            if (userId != null) {
                userId.getArticleCollection().add(article);
                userId = em.merge(userId);
            }
            for (Comment commentCollectionComment : article.getCommentCollection()) {
                Article oldArticleIdOfCommentCollectionComment = commentCollectionComment.getArticleId();
                commentCollectionComment.setArticleId(article);
                commentCollectionComment = em.merge(commentCollectionComment);
                if (oldArticleIdOfCommentCollectionComment != null) {
                    oldArticleIdOfCommentCollectionComment.getCommentCollection().remove(commentCollectionComment);
                    oldArticleIdOfCommentCollectionComment = em.merge(oldArticleIdOfCommentCollectionComment);
                }
            }
            for (Emotion emotionCollectionEmotion : article.getEmotionCollection()) {
                Article oldArticleOfEmotionCollectionEmotion = emotionCollectionEmotion.getArticle();
                emotionCollectionEmotion.setArticle(article);
                emotionCollectionEmotion = em.merge(emotionCollectionEmotion);
                if (oldArticleOfEmotionCollectionEmotion != null) {
                    oldArticleOfEmotionCollectionEmotion.getEmotionCollection().remove(emotionCollectionEmotion);
                    oldArticleOfEmotionCollectionEmotion = em.merge(oldArticleOfEmotionCollectionEmotion);
                }
            }
            for (Notification notificationCollectionNotification : article.getNotificationCollection()) {
                Article oldArticleIdOfNotificationCollectionNotification = notificationCollectionNotification.getArticleId();
                notificationCollectionNotification.setArticleId(article);
                notificationCollectionNotification = em.merge(notificationCollectionNotification);
                if (oldArticleIdOfNotificationCollectionNotification != null) {
                    oldArticleIdOfNotificationCollectionNotification.getNotificationCollection().remove(notificationCollectionNotification);
                    oldArticleIdOfNotificationCollectionNotification = em.merge(oldArticleIdOfNotificationCollectionNotification);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Article article) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Article persistentArticle = em.find(Article.class, article.getArticleId());
            Users userIdOld = persistentArticle.getUserId();
            Users userIdNew = article.getUserId();
            Collection<Comment> commentCollectionOld = persistentArticle.getCommentCollection();
            Collection<Comment> commentCollectionNew = article.getCommentCollection();
            Collection<Emotion> emotionCollectionOld = persistentArticle.getEmotionCollection();
            Collection<Emotion> emotionCollectionNew = article.getEmotionCollection();
            Collection<Notification> notificationCollectionOld = persistentArticle.getNotificationCollection();
            Collection<Notification> notificationCollectionNew = article.getNotificationCollection();
            List<String> illegalOrphanMessages = null;
            for (Comment commentCollectionOldComment : commentCollectionOld) {
                if (!commentCollectionNew.contains(commentCollectionOldComment)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Comment " + commentCollectionOldComment + " since its articleId field is not nullable.");
                }
            }
            for (Emotion emotionCollectionOldEmotion : emotionCollectionOld) {
                if (!emotionCollectionNew.contains(emotionCollectionOldEmotion)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Emotion " + emotionCollectionOldEmotion + " since its article field is not nullable.");
                }
            }
            for (Notification notificationCollectionOldNotification : notificationCollectionOld) {
                if (!notificationCollectionNew.contains(notificationCollectionOldNotification)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Notification " + notificationCollectionOldNotification + " since its articleId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (userIdNew != null) {
                userIdNew = em.getReference(userIdNew.getClass(), userIdNew.getUserId());
                article.setUserId(userIdNew);
            }
            Collection<Comment> attachedCommentCollectionNew = new ArrayList<Comment>();
            for (Comment commentCollectionNewCommentToAttach : commentCollectionNew) {
                commentCollectionNewCommentToAttach = em.getReference(commentCollectionNewCommentToAttach.getClass(), commentCollectionNewCommentToAttach.getCommentId());
                attachedCommentCollectionNew.add(commentCollectionNewCommentToAttach);
            }
            commentCollectionNew = attachedCommentCollectionNew;
            article.setCommentCollection(commentCollectionNew);
            Collection<Emotion> attachedEmotionCollectionNew = new ArrayList<Emotion>();
            for (Emotion emotionCollectionNewEmotionToAttach : emotionCollectionNew) {
                emotionCollectionNewEmotionToAttach = em.getReference(emotionCollectionNewEmotionToAttach.getClass(), emotionCollectionNewEmotionToAttach.getEmotionPK());
                attachedEmotionCollectionNew.add(emotionCollectionNewEmotionToAttach);
            }
            emotionCollectionNew = attachedEmotionCollectionNew;
            article.setEmotionCollection(emotionCollectionNew);
            Collection<Notification> attachedNotificationCollectionNew = new ArrayList<Notification>();
            for (Notification notificationCollectionNewNotificationToAttach : notificationCollectionNew) {
                notificationCollectionNewNotificationToAttach = em.getReference(notificationCollectionNewNotificationToAttach.getClass(), notificationCollectionNewNotificationToAttach.getNotificationId());
                attachedNotificationCollectionNew.add(notificationCollectionNewNotificationToAttach);
            }
            notificationCollectionNew = attachedNotificationCollectionNew;
            article.setNotificationCollection(notificationCollectionNew);
            article = em.merge(article);
            if (userIdOld != null && !userIdOld.equals(userIdNew)) {
                userIdOld.getArticleCollection().remove(article);
                userIdOld = em.merge(userIdOld);
            }
            if (userIdNew != null && !userIdNew.equals(userIdOld)) {
                userIdNew.getArticleCollection().add(article);
                userIdNew = em.merge(userIdNew);
            }
            for (Comment commentCollectionNewComment : commentCollectionNew) {
                if (!commentCollectionOld.contains(commentCollectionNewComment)) {
                    Article oldArticleIdOfCommentCollectionNewComment = commentCollectionNewComment.getArticleId();
                    commentCollectionNewComment.setArticleId(article);
                    commentCollectionNewComment = em.merge(commentCollectionNewComment);
                    if (oldArticleIdOfCommentCollectionNewComment != null && !oldArticleIdOfCommentCollectionNewComment.equals(article)) {
                        oldArticleIdOfCommentCollectionNewComment.getCommentCollection().remove(commentCollectionNewComment);
                        oldArticleIdOfCommentCollectionNewComment = em.merge(oldArticleIdOfCommentCollectionNewComment);
                    }
                }
            }
            for (Emotion emotionCollectionNewEmotion : emotionCollectionNew) {
                if (!emotionCollectionOld.contains(emotionCollectionNewEmotion)) {
                    Article oldArticleOfEmotionCollectionNewEmotion = emotionCollectionNewEmotion.getArticle();
                    emotionCollectionNewEmotion.setArticle(article);
                    emotionCollectionNewEmotion = em.merge(emotionCollectionNewEmotion);
                    if (oldArticleOfEmotionCollectionNewEmotion != null && !oldArticleOfEmotionCollectionNewEmotion.equals(article)) {
                        oldArticleOfEmotionCollectionNewEmotion.getEmotionCollection().remove(emotionCollectionNewEmotion);
                        oldArticleOfEmotionCollectionNewEmotion = em.merge(oldArticleOfEmotionCollectionNewEmotion);
                    }
                }
            }
            for (Notification notificationCollectionNewNotification : notificationCollectionNew) {
                if (!notificationCollectionOld.contains(notificationCollectionNewNotification)) {
                    Article oldArticleIdOfNotificationCollectionNewNotification = notificationCollectionNewNotification.getArticleId();
                    notificationCollectionNewNotification.setArticleId(article);
                    notificationCollectionNewNotification = em.merge(notificationCollectionNewNotification);
                    if (oldArticleIdOfNotificationCollectionNewNotification != null && !oldArticleIdOfNotificationCollectionNewNotification.equals(article)) {
                        oldArticleIdOfNotificationCollectionNewNotification.getNotificationCollection().remove(notificationCollectionNewNotification);
                        oldArticleIdOfNotificationCollectionNewNotification = em.merge(oldArticleIdOfNotificationCollectionNewNotification);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = article.getArticleId();
                if (findArticle(id) == null) {
                    throw new NonexistentEntityException("The article with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Article article;
            try {
                article = em.getReference(Article.class, id);
                article.getArticleId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The article with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Comment> commentCollectionOrphanCheck = article.getCommentCollection();
            for (Comment commentCollectionOrphanCheckComment : commentCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Article (" + article + ") cannot be destroyed since the Comment " + commentCollectionOrphanCheckComment + " in its commentCollection field has a non-nullable articleId field.");
            }
            Collection<Emotion> emotionCollectionOrphanCheck = article.getEmotionCollection();
            for (Emotion emotionCollectionOrphanCheckEmotion : emotionCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Article (" + article + ") cannot be destroyed since the Emotion " + emotionCollectionOrphanCheckEmotion + " in its emotionCollection field has a non-nullable article field.");
            }
            Collection<Notification> notificationCollectionOrphanCheck = article.getNotificationCollection();
            for (Notification notificationCollectionOrphanCheckNotification : notificationCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Article (" + article + ") cannot be destroyed since the Notification " + notificationCollectionOrphanCheckNotification + " in its notificationCollection field has a non-nullable articleId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Users userId = article.getUserId();
            if (userId != null) {
                userId.getArticleCollection().remove(article);
                userId = em.merge(userId);
            }
            em.remove(article);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Article> findArticleEntities() {
        return findArticleEntities(true, -1, -1);
    }

    public List<Article> findArticleEntities(int maxResults, int firstResult) {
        return findArticleEntities(false, maxResults, firstResult);
    }

    private List<Article> findArticleEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Article.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Article findArticle(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Article.class, id);
        } finally {
            em.close();
        }
    }

    public int getArticleCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Article> rt = cq.from(Article.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
