package com.duy.social.network.mail;

public class MailMessage {

    private final String destMail;
    private final String subject;
    private final String content;

    public MailMessage(String destMail, String subject, String content) {
        this.destMail = destMail;
        this.subject = subject;
        this.content = content;
    }

    public String getDestMail() {
        return destMail;
    }

    public String getContent() {
        return content;
    }

    public String getSubject() {
        return subject;
    }
}
