/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duy.social.network.notification;

import com.duy.social.network.comment.CommentBLO;
import com.duy.social.network.jpa.entity.Comment;
import com.duy.social.network.jpa.entity.Notification;
import com.duy.social.network.jpa.entity.Users;
import com.duy.social.network.utils.Constants;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author duy
 */
public class NotificationController extends HttpServlet {


    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String url = Constants.ERROR_PAGE;
        try {
            HttpSession session = request.getSession(true);
            Users user = (Users) session.getAttribute("user");
            if (user == null) {
                url = Constants.LOGIN_PAGE;

            } else {
                NotificationBLO notificationBLO = new NotificationBLO();
                CommentBLO commentBLO = new CommentBLO();

                // In general, this method is better than using jsp to generate notification message. It easy to support
                // internalization.
                // https://blog.osmosys.asia/2017/04/26/design-notification-system/
                List<Notification> rawNotifications = notificationBLO.getActiveNotifications(user);
                List<NotificationObject> notifications = new ArrayList<>(rawNotifications.size());

                // TODO: 9/30/2020 use batch query to improve perforamnce
                for (Notification rawNotification : rawNotifications) {
                    NotificationObject notificationObject = new NotificationObject(rawNotification.getNotificationId(),
                            rawNotification.getArticleId(), rawNotification.getDate());
                    notifications.add(notificationObject);

                    int notificationType = rawNotification.getNotificationType();
                    if (notificationType == NotificationBLO.LIKE) {
                        notificationObject.setMessage(String.format("%s did like your post '%s'",
                                rawNotification.getUserId().getFullName(), rawNotification.getArticleId().getTitle()));

                    } else if (notificationType == NotificationBLO.DISLIKE) {
                        notificationObject.setMessage(String.format("%s did dislike your post '%s'",
                                rawNotification.getUserId().getFullName(), rawNotification.getArticleId().getTitle()));

                    } else if (notificationType == NotificationBLO.COMMENT) {
                        Comment comment = commentBLO.get(rawNotification.getObjectId());
                        if (comment != null) {
                            notificationObject.setMessage(String.format("%s commented '%s' on your post '%s'",
                                    rawNotification.getUserId().getFullName(), comment.getCommentId(), rawNotification.getArticleId().getTitle()));
                        }
                    }
                }
                request.setAttribute("notifications", notifications);
                url = Constants.NOTIFICATION_PAGE;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        request.getRequestDispatcher(url).forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
