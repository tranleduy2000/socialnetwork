/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duy.social.network.jpa.controller;

import com.duy.social.network.jpa.controller.exceptions.IllegalOrphanException;
import com.duy.social.network.jpa.controller.exceptions.NonexistentEntityException;
import com.duy.social.network.jpa.controller.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.duy.social.network.jpa.entity.Role;
import com.duy.social.network.jpa.entity.Comment;
import java.util.ArrayList;
import java.util.Collection;
import com.duy.social.network.jpa.entity.Article;
import com.duy.social.network.jpa.entity.Emotion;
import com.duy.social.network.jpa.entity.Notification;
import com.duy.social.network.jpa.entity.Users;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author duy
 */
public class UsersJpaController implements Serializable {

    public UsersJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Users users) throws PreexistingEntityException, Exception {
        if (users.getCommentCollection() == null) {
            users.setCommentCollection(new ArrayList<Comment>());
        }
        if (users.getArticleCollection() == null) {
            users.setArticleCollection(new ArrayList<Article>());
        }
        if (users.getEmotionCollection() == null) {
            users.setEmotionCollection(new ArrayList<Emotion>());
        }
        if (users.getNotificationCollection() == null) {
            users.setNotificationCollection(new ArrayList<Notification>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Role roleId = users.getRoleId();
            if (roleId != null) {
                roleId = em.getReference(roleId.getClass(), roleId.getRoleId());
                users.setRoleId(roleId);
            }
            Collection<Comment> attachedCommentCollection = new ArrayList<Comment>();
            for (Comment commentCollectionCommentToAttach : users.getCommentCollection()) {
                commentCollectionCommentToAttach = em.getReference(commentCollectionCommentToAttach.getClass(), commentCollectionCommentToAttach.getCommentId());
                attachedCommentCollection.add(commentCollectionCommentToAttach);
            }
            users.setCommentCollection(attachedCommentCollection);
            Collection<Article> attachedArticleCollection = new ArrayList<Article>();
            for (Article articleCollectionArticleToAttach : users.getArticleCollection()) {
                articleCollectionArticleToAttach = em.getReference(articleCollectionArticleToAttach.getClass(), articleCollectionArticleToAttach.getArticleId());
                attachedArticleCollection.add(articleCollectionArticleToAttach);
            }
            users.setArticleCollection(attachedArticleCollection);
            Collection<Emotion> attachedEmotionCollection = new ArrayList<Emotion>();
            for (Emotion emotionCollectionEmotionToAttach : users.getEmotionCollection()) {
                emotionCollectionEmotionToAttach = em.getReference(emotionCollectionEmotionToAttach.getClass(), emotionCollectionEmotionToAttach.getEmotionPK());
                attachedEmotionCollection.add(emotionCollectionEmotionToAttach);
            }
            users.setEmotionCollection(attachedEmotionCollection);
            Collection<Notification> attachedNotificationCollection = new ArrayList<Notification>();
            for (Notification notificationCollectionNotificationToAttach : users.getNotificationCollection()) {
                notificationCollectionNotificationToAttach = em.getReference(notificationCollectionNotificationToAttach.getClass(), notificationCollectionNotificationToAttach.getNotificationId());
                attachedNotificationCollection.add(notificationCollectionNotificationToAttach);
            }
            users.setNotificationCollection(attachedNotificationCollection);
            em.persist(users);
            if (roleId != null) {
                roleId.getUsersCollection().add(users);
                roleId = em.merge(roleId);
            }
            for (Comment commentCollectionComment : users.getCommentCollection()) {
                Users oldUserIdOfCommentCollectionComment = commentCollectionComment.getUserId();
                commentCollectionComment.setUserId(users);
                commentCollectionComment = em.merge(commentCollectionComment);
                if (oldUserIdOfCommentCollectionComment != null) {
                    oldUserIdOfCommentCollectionComment.getCommentCollection().remove(commentCollectionComment);
                    oldUserIdOfCommentCollectionComment = em.merge(oldUserIdOfCommentCollectionComment);
                }
            }
            for (Article articleCollectionArticle : users.getArticleCollection()) {
                Users oldUserIdOfArticleCollectionArticle = articleCollectionArticle.getUserId();
                articleCollectionArticle.setUserId(users);
                articleCollectionArticle = em.merge(articleCollectionArticle);
                if (oldUserIdOfArticleCollectionArticle != null) {
                    oldUserIdOfArticleCollectionArticle.getArticleCollection().remove(articleCollectionArticle);
                    oldUserIdOfArticleCollectionArticle = em.merge(oldUserIdOfArticleCollectionArticle);
                }
            }
            for (Emotion emotionCollectionEmotion : users.getEmotionCollection()) {
                Users oldUsersOfEmotionCollectionEmotion = emotionCollectionEmotion.getUsers();
                emotionCollectionEmotion.setUsers(users);
                emotionCollectionEmotion = em.merge(emotionCollectionEmotion);
                if (oldUsersOfEmotionCollectionEmotion != null) {
                    oldUsersOfEmotionCollectionEmotion.getEmotionCollection().remove(emotionCollectionEmotion);
                    oldUsersOfEmotionCollectionEmotion = em.merge(oldUsersOfEmotionCollectionEmotion);
                }
            }
            for (Notification notificationCollectionNotification : users.getNotificationCollection()) {
                Users oldUserIdOfNotificationCollectionNotification = notificationCollectionNotification.getUserId();
                notificationCollectionNotification.setUserId(users);
                notificationCollectionNotification = em.merge(notificationCollectionNotification);
                if (oldUserIdOfNotificationCollectionNotification != null) {
                    oldUserIdOfNotificationCollectionNotification.getNotificationCollection().remove(notificationCollectionNotification);
                    oldUserIdOfNotificationCollectionNotification = em.merge(oldUserIdOfNotificationCollectionNotification);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findUsers(users.getUserId()) != null) {
                throw new PreexistingEntityException("Users " + users + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Users users) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Users persistentUsers = em.find(Users.class, users.getUserId());
            Role roleIdOld = persistentUsers.getRoleId();
            Role roleIdNew = users.getRoleId();
            Collection<Comment> commentCollectionOld = persistentUsers.getCommentCollection();
            Collection<Comment> commentCollectionNew = users.getCommentCollection();
            Collection<Article> articleCollectionOld = persistentUsers.getArticleCollection();
            Collection<Article> articleCollectionNew = users.getArticleCollection();
            Collection<Emotion> emotionCollectionOld = persistentUsers.getEmotionCollection();
            Collection<Emotion> emotionCollectionNew = users.getEmotionCollection();
            Collection<Notification> notificationCollectionOld = persistentUsers.getNotificationCollection();
            Collection<Notification> notificationCollectionNew = users.getNotificationCollection();
            List<String> illegalOrphanMessages = null;
            for (Comment commentCollectionOldComment : commentCollectionOld) {
                if (!commentCollectionNew.contains(commentCollectionOldComment)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Comment " + commentCollectionOldComment + " since its userId field is not nullable.");
                }
            }
            for (Article articleCollectionOldArticle : articleCollectionOld) {
                if (!articleCollectionNew.contains(articleCollectionOldArticle)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Article " + articleCollectionOldArticle + " since its userId field is not nullable.");
                }
            }
            for (Emotion emotionCollectionOldEmotion : emotionCollectionOld) {
                if (!emotionCollectionNew.contains(emotionCollectionOldEmotion)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Emotion " + emotionCollectionOldEmotion + " since its users field is not nullable.");
                }
            }
            for (Notification notificationCollectionOldNotification : notificationCollectionOld) {
                if (!notificationCollectionNew.contains(notificationCollectionOldNotification)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Notification " + notificationCollectionOldNotification + " since its userId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (roleIdNew != null) {
                roleIdNew = em.getReference(roleIdNew.getClass(), roleIdNew.getRoleId());
                users.setRoleId(roleIdNew);
            }
            Collection<Comment> attachedCommentCollectionNew = new ArrayList<Comment>();
            for (Comment commentCollectionNewCommentToAttach : commentCollectionNew) {
                commentCollectionNewCommentToAttach = em.getReference(commentCollectionNewCommentToAttach.getClass(), commentCollectionNewCommentToAttach.getCommentId());
                attachedCommentCollectionNew.add(commentCollectionNewCommentToAttach);
            }
            commentCollectionNew = attachedCommentCollectionNew;
            users.setCommentCollection(commentCollectionNew);
            Collection<Article> attachedArticleCollectionNew = new ArrayList<Article>();
            for (Article articleCollectionNewArticleToAttach : articleCollectionNew) {
                articleCollectionNewArticleToAttach = em.getReference(articleCollectionNewArticleToAttach.getClass(), articleCollectionNewArticleToAttach.getArticleId());
                attachedArticleCollectionNew.add(articleCollectionNewArticleToAttach);
            }
            articleCollectionNew = attachedArticleCollectionNew;
            users.setArticleCollection(articleCollectionNew);
            Collection<Emotion> attachedEmotionCollectionNew = new ArrayList<Emotion>();
            for (Emotion emotionCollectionNewEmotionToAttach : emotionCollectionNew) {
                emotionCollectionNewEmotionToAttach = em.getReference(emotionCollectionNewEmotionToAttach.getClass(), emotionCollectionNewEmotionToAttach.getEmotionPK());
                attachedEmotionCollectionNew.add(emotionCollectionNewEmotionToAttach);
            }
            emotionCollectionNew = attachedEmotionCollectionNew;
            users.setEmotionCollection(emotionCollectionNew);
            Collection<Notification> attachedNotificationCollectionNew = new ArrayList<Notification>();
            for (Notification notificationCollectionNewNotificationToAttach : notificationCollectionNew) {
                notificationCollectionNewNotificationToAttach = em.getReference(notificationCollectionNewNotificationToAttach.getClass(), notificationCollectionNewNotificationToAttach.getNotificationId());
                attachedNotificationCollectionNew.add(notificationCollectionNewNotificationToAttach);
            }
            notificationCollectionNew = attachedNotificationCollectionNew;
            users.setNotificationCollection(notificationCollectionNew);
            users = em.merge(users);
            if (roleIdOld != null && !roleIdOld.equals(roleIdNew)) {
                roleIdOld.getUsersCollection().remove(users);
                roleIdOld = em.merge(roleIdOld);
            }
            if (roleIdNew != null && !roleIdNew.equals(roleIdOld)) {
                roleIdNew.getUsersCollection().add(users);
                roleIdNew = em.merge(roleIdNew);
            }
            for (Comment commentCollectionNewComment : commentCollectionNew) {
                if (!commentCollectionOld.contains(commentCollectionNewComment)) {
                    Users oldUserIdOfCommentCollectionNewComment = commentCollectionNewComment.getUserId();
                    commentCollectionNewComment.setUserId(users);
                    commentCollectionNewComment = em.merge(commentCollectionNewComment);
                    if (oldUserIdOfCommentCollectionNewComment != null && !oldUserIdOfCommentCollectionNewComment.equals(users)) {
                        oldUserIdOfCommentCollectionNewComment.getCommentCollection().remove(commentCollectionNewComment);
                        oldUserIdOfCommentCollectionNewComment = em.merge(oldUserIdOfCommentCollectionNewComment);
                    }
                }
            }
            for (Article articleCollectionNewArticle : articleCollectionNew) {
                if (!articleCollectionOld.contains(articleCollectionNewArticle)) {
                    Users oldUserIdOfArticleCollectionNewArticle = articleCollectionNewArticle.getUserId();
                    articleCollectionNewArticle.setUserId(users);
                    articleCollectionNewArticle = em.merge(articleCollectionNewArticle);
                    if (oldUserIdOfArticleCollectionNewArticle != null && !oldUserIdOfArticleCollectionNewArticle.equals(users)) {
                        oldUserIdOfArticleCollectionNewArticle.getArticleCollection().remove(articleCollectionNewArticle);
                        oldUserIdOfArticleCollectionNewArticle = em.merge(oldUserIdOfArticleCollectionNewArticle);
                    }
                }
            }
            for (Emotion emotionCollectionNewEmotion : emotionCollectionNew) {
                if (!emotionCollectionOld.contains(emotionCollectionNewEmotion)) {
                    Users oldUsersOfEmotionCollectionNewEmotion = emotionCollectionNewEmotion.getUsers();
                    emotionCollectionNewEmotion.setUsers(users);
                    emotionCollectionNewEmotion = em.merge(emotionCollectionNewEmotion);
                    if (oldUsersOfEmotionCollectionNewEmotion != null && !oldUsersOfEmotionCollectionNewEmotion.equals(users)) {
                        oldUsersOfEmotionCollectionNewEmotion.getEmotionCollection().remove(emotionCollectionNewEmotion);
                        oldUsersOfEmotionCollectionNewEmotion = em.merge(oldUsersOfEmotionCollectionNewEmotion);
                    }
                }
            }
            for (Notification notificationCollectionNewNotification : notificationCollectionNew) {
                if (!notificationCollectionOld.contains(notificationCollectionNewNotification)) {
                    Users oldUserIdOfNotificationCollectionNewNotification = notificationCollectionNewNotification.getUserId();
                    notificationCollectionNewNotification.setUserId(users);
                    notificationCollectionNewNotification = em.merge(notificationCollectionNewNotification);
                    if (oldUserIdOfNotificationCollectionNewNotification != null && !oldUserIdOfNotificationCollectionNewNotification.equals(users)) {
                        oldUserIdOfNotificationCollectionNewNotification.getNotificationCollection().remove(notificationCollectionNewNotification);
                        oldUserIdOfNotificationCollectionNewNotification = em.merge(oldUserIdOfNotificationCollectionNewNotification);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = users.getUserId();
                if (findUsers(id) == null) {
                    throw new NonexistentEntityException("The users with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Users users;
            try {
                users = em.getReference(Users.class, id);
                users.getUserId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The users with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Comment> commentCollectionOrphanCheck = users.getCommentCollection();
            for (Comment commentCollectionOrphanCheckComment : commentCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Users (" + users + ") cannot be destroyed since the Comment " + commentCollectionOrphanCheckComment + " in its commentCollection field has a non-nullable userId field.");
            }
            Collection<Article> articleCollectionOrphanCheck = users.getArticleCollection();
            for (Article articleCollectionOrphanCheckArticle : articleCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Users (" + users + ") cannot be destroyed since the Article " + articleCollectionOrphanCheckArticle + " in its articleCollection field has a non-nullable userId field.");
            }
            Collection<Emotion> emotionCollectionOrphanCheck = users.getEmotionCollection();
            for (Emotion emotionCollectionOrphanCheckEmotion : emotionCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Users (" + users + ") cannot be destroyed since the Emotion " + emotionCollectionOrphanCheckEmotion + " in its emotionCollection field has a non-nullable users field.");
            }
            Collection<Notification> notificationCollectionOrphanCheck = users.getNotificationCollection();
            for (Notification notificationCollectionOrphanCheckNotification : notificationCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Users (" + users + ") cannot be destroyed since the Notification " + notificationCollectionOrphanCheckNotification + " in its notificationCollection field has a non-nullable userId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Role roleId = users.getRoleId();
            if (roleId != null) {
                roleId.getUsersCollection().remove(users);
                roleId = em.merge(roleId);
            }
            em.remove(users);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Users> findUsersEntities() {
        return findUsersEntities(true, -1, -1);
    }

    public List<Users> findUsersEntities(int maxResults, int firstResult) {
        return findUsersEntities(false, maxResults, firstResult);
    }

    private List<Users> findUsersEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Users.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Users findUsers(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Users.class, id);
        } finally {
            em.close();
        }
    }

    public int getUsersCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Users> rt = cq.from(Users.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
