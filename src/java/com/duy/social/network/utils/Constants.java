package com.duy.social.network.utils;

import com.duy.social.network.article.ArticleController;
import com.duy.social.network.article.ArticleDetailsController;
import com.duy.social.network.article.CreateArticleController;
import com.duy.social.network.article.DeleteArticleController;
import com.duy.social.network.comment.CreateCommentController;
import com.duy.social.network.comment.DeleteCommentController;
import com.duy.social.network.emotion.CreateEmotionController;
import com.duy.social.network.notification.DeleteNotificationController;
import com.duy.social.network.notification.NotificationController;
import com.duy.social.network.user.*;

public class Constants {

    public static final String ERROR_PAGE = "error.jsp";

    public static final String LOGIN_PAGE = "login.jsp";
    public static final String CREATE_ACCOUNT_PAGE = "createAccount.jsp";
    public static final String SEND_VERIFICATION_CODE = SendVerificationCodeController.class.getSimpleName();
    public static final String ENTER_VERIFICATION_CODE_PAGE = "verifyEmail.jsp";

    public static final String PROFILE_PAGE = "userDetails.jsp";

    public static final String ARTICLE = ArticleController.class.getSimpleName();
    public static final String ARTICLE_PAGE = "article.jsp";
    public static final String ARTICLE_DETAILS = ArticleDetailsController.class.getSimpleName();
    public static final String ARTICLE_DETAILS_PAGE = "articleDetails.jsp";

    public static final String NOTIFICATION = NotificationController.class.getSimpleName();
    public static final String NOTIFICATION_PAGE = "notification.jsp";

    public static final String CREATE_ACCOUNT = CreateAccountController.class.getSimpleName();
    public static final String LOGIN = LoginController.class.getSimpleName();
    public static final String LOGOUT = LogoutController.class.getSimpleName();
    public static final String CREATE_ARTICLE = CreateArticleController.class.getSimpleName();
    public static final String VIEW_ARTICLES = ArticleController.class.getSimpleName();
    public static final String CREATE_COMMENT = CreateCommentController.class.getSimpleName();
    public static final String DELETE_COMMENT = DeleteCommentController.class.getSimpleName();
    public static final String DELETE_ARTICLE = DeleteArticleController.class.getSimpleName();
    public static final String CREATE_EMOTION = CreateEmotionController.class.getSimpleName();
    public static final String VIEW_NOTIFICATION = NotificationController.class.getSimpleName();
    public static final String DELETE_NOTIFICATION = DeleteNotificationController.class.getSimpleName();
    public static final String UPDATE_ACCOUNT = UpdateAccountController.class.getSimpleName();
    public static final String UPDATE_PASSWORD = UpdatePasswordController.class.getSimpleName();
    public static final String VERIFY_CODE = VerifyCodeController.class.getSimpleName();

    public static final String USER_VERIFICATION_PAGE = "verifyEmail.jsp";

}
