package com.duy.social.network.utils;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DbUtils {
    private static final String PERSISTENCE_NAME = "SocialNetworkPU";

    private static EntityManagerFactory entityManagerFactory;

    public static EntityManagerFactory getInstance() {
        if (entityManagerFactory == null) {
            entityManagerFactory = Persistence.createEntityManagerFactory(PERSISTENCE_NAME);
        }
        return entityManagerFactory;
    }
}
