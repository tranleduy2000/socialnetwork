package com.duy.social.network.notification;

import com.duy.social.network.jpa.entity.Article;

import java.util.Date;

public class NotificationObject {
    private int notificationId;
    private String message;
    private Date date;
    private Article article;


    public NotificationObject(Integer notificationId, Article article, Date date) {
        this.notificationId = notificationId;
        this.date = date;
        this.article = article;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public int getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(int notificationId) {
        this.notificationId = notificationId;
    }
}
