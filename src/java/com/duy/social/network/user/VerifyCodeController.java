/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duy.social.network.user;

import com.duy.social.network.jpa.entity.Users;
import com.duy.social.network.user.blo.UserBLO;
import com.duy.social.network.utils.Constants;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author duy
 */
public class VerifyCodeController extends HttpServlet {


    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        String url = Constants.ERROR_PAGE;
        try {
            HttpSession session = request.getSession(true);
            Users user = (Users) session.getAttribute("user");
            if (user == null) {
                url = Constants.LOGIN_PAGE;

            } else if (user.getStatus().equalsIgnoreCase(UserBLO.STATUS_ACTIVE)) {
                url = Constants.ARTICLE;

            } else {

                String enteredCode = request.getParameter("txtEnteredCode");
                UserBLO userBLO = new UserBLO();

                if (userBLO.checkVerificationCode(user.getUserId(), enteredCode)) {
                    user = userBLO.get(user.getUserId());
                    session.setAttribute("user", user); // update user info
                    url = Constants.ARTICLE;

                } else {
                    request.setAttribute("error", "Wrong verification code");
                    url = Constants.ENTER_VERIFICATION_CODE_PAGE;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        request.getRequestDispatcher(url).forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
