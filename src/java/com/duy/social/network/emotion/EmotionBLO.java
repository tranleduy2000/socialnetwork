package com.duy.social.network.emotion;

import com.duy.social.network.jpa.controller.ArticleJpaController;
import com.duy.social.network.jpa.controller.EmotionJpaController;
import com.duy.social.network.jpa.controller.UsersJpaController;
import com.duy.social.network.jpa.controller.exceptions.NonexistentEntityException;
import com.duy.social.network.jpa.entity.Article;
import com.duy.social.network.jpa.entity.Emotion;
import com.duy.social.network.jpa.entity.EmotionPK;
import com.duy.social.network.jpa.entity.Users;
import com.duy.social.network.utils.DbUtils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public class EmotionBLO {

    public static final int LIKE = 1;
    public static final int DISLIKE = 2;

    private final EntityManagerFactory entityManagerFactory = DbUtils.getInstance();

    /**
     * @param emotionType 0: none, 1:like, 2: dislike
     */
    public boolean editOrCreateIfExist(Users user, int articleId, int emotionType) {
        try {
            EmotionJpaController emotionJpaController = new EmotionJpaController(entityManagerFactory);
            Emotion emotion = new Emotion(new EmotionPK(user.getUserId(), articleId));
            emotion.setEmotionType(emotionType);
            emotion.setUsers(new UsersJpaController(entityManagerFactory).findUsers(user.getUserId()));
            emotion.setArticle(new ArticleJpaController(entityManagerFactory).findArticle(articleId));

            try {
                emotionJpaController.edit(emotion);
            } catch (NonexistentEntityException e) {
                emotionJpaController.create(emotion);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public Emotion getEmotion(Users user, Article article) {
        EmotionJpaController emotionJpaController = new EmotionJpaController(entityManagerFactory);
        return emotionJpaController.findEmotion(new EmotionPK(user.getUserId(), article.getArticleId()));
    }


    public int getNumberOfEmotions(Article article, int emotionType) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            return entityManager.createQuery("SELECT COUNT(e) " +
                    "FROM Emotion e " +
                    "WHERE e.emotionPK.articleId = :articleId AND e.emotionType = :emotionType", Long.class)
                    .setParameter("articleId", article.getArticleId())
                    .setParameter("emotionType", emotionType)
                    .getSingleResult().intValue();
        } finally {
            entityManager.close();
        }
    }
}
