package com.duy.social.network.mail;

public class MailAccount {
    private final String email;
    private final String password;

    public MailAccount(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
