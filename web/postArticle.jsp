<%-- 
    Document   : postArticle
    Created on : Sep 19, 2020, 3:13:57 PM
    Author     : duy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Post article Page</title>
    <link rel="stylesheet" href="css/style.css">
    <jsp:include page="metadata.jsp"/>
</head>
<body>
<nav class="navbar navbar-light bg-light">
    <span class="navbar-brand">Post article</span>
    <jsp:include page="include/navigation.jsp"/>
</nav>
<div class="container">
    <form method="post" action="CreateArticleController" enctype="multipart/form-data">
        <div class="form-group">
            <label for="txtTitle">Title</label><input id="txtTitle" type="text" maxlength="200" required
                                                      name="txtTitle" class="form-control">

        </div>
        <div class="form-group">
            <label for="txtDescription">Description</label><textarea type="text" maxlength="1000"
                                                                     name="txtDescription" id="txtDescription"
                                                                     rows="3" class="form-control"></textarea>
        </div>
        <div class="form-group">
            <label for="txtImageFile"> Select a file to upload:</label><input type="file" name="txtImageFile"
                                                                              id="txtImageFile"
                                                                              class="form-control-file">
        </div>
        <input type="submit" name="btnAction" value="Post article" class="btn btn-primary">
        <input type="reset" class="btn btn-warning">
    </form>
</div>
</body>
</html>
