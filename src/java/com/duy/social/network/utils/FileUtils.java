package com.duy.social.network.utils;

public class FileUtils {
    public static boolean isImageFile(String path) {
        path = path.toLowerCase();
        return path.endsWith(".png") || path.endsWith(".jpg")
                || path.endsWith(".jpeg")
                || path.endsWith(".webp");
    }
}
