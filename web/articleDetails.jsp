<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
    Document   : articleDetails
    Created on : Sep 19, 2020, 3:44:05 PM
    Author     : duy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>${article.title}</title>
    <script type="text/javascript" src="js/main.js"></script>
    <link href="css/style.css" rel="stylesheet">
    <jsp:include page="metadata.jsp"/>
</head>
<body>
<nav class="navbar navbar-light bg-light">
    <%--Welcom function--%>
    <span class="navbar-brand">Hello, ${sessionScope.user.fullName}</span>
    <jsp:include page="include/navigation.jsp"/>
</nav>
<c:if test="${requestScope.article != null}">
    <c:set var="article" scope="request" value="${requestScope.article}"/>
    <div class="container w-50">
        <div class="card my-2">
            <div class="card-header">
                <span class="font-weight-bold">${article.userId.fullName}</span><br>
                <span class="small">${article.date}</span>
            </div>
            <div class="card-body">
                <span class="card-title font-weight-bold">${article.title}</span>
                <p class="card-text">${article.description}</p>
                <c:if test="${not empty article.imagePath}">
                    <div class="card-img article-image">
                        <img class="mw-100" src="${pageContext.servletContext.contextPath}${article.imagePath}"
                             alt="${article.title}">
                    </div>
                </c:if>
                <c:if test="${sessionScope.user.userId == article.userId.userId || sessionScope.user.roleId.roleId == 1}">
                    <button class="btn btn-warning"
                            onclick="showDeleteArticleConfirmationDialog('${article.articleId}')">
                        <i class="fa fa-remove"></i>Delete
                    </button>
                </c:if>
            </div>
        </div>
        <div class="my-2">
            <a class="btn btn-outline-primary article-button-emotion"
               id="btnLike"
               href="MainController?btnAction=CreateEmotion&txtArticleId=${article.articleId}&txtEmotionType=${1}">
                <i class="fa fa-thumbs-up"></i>Like (${requestScope.totalLike})</a>
            <c:if test="${requestScope.emotion != null && requestScope.emotion.emotionType == 1}">
                <script>
                    $(document).ready(function () {
                        $("#btnLike").removeClass("btn-outline-primary")
                        $("#btnLike").addClass("btn-primary")
                    })
                </script>
            </c:if>
            <a class="btn btn-outline-primary article-button-emotion"
               id="btnDislike"
               href="MainController?btnAction=CreateEmotion&txtArticleId=${article.articleId}&txtEmotionType=${2}">
                <i class="fa fa-thumbs-down"></i>Dislike (${requestScope.totalDislike})</a>
            <c:if test="${requestScope.emotion != null && requestScope.emotion.emotionType == 2}">
                <script>
                    $(document).ready(function () {
                        $("#btnDislike").removeClass("btn-outline-primary")
                        $("#btnDislike").addClass("btn-primary")
                    })
                </script>
            </c:if>
                <%--Only member can use this function--%>
            <c:if test="${sessionScope.user.roleId.roleId != 0}">
                <script>
                    $(document).ready(function () {
                        $("#btnLike").addClass("disabled")
                        $("#btnDislike").addClass("disabled")
                    })
                </script>
            </c:if>
        </div>
            <%--Only member can use this function--%>
        <c:if test="${sessionScope.user.roleId.roleId == 0}">
            <div class="my-2">
                <form method="post" action="MainController" class="form-inline">
                    <input type="hidden" name="txtArticleId" value="${article.articleId}">
                    <div class="form-group">
                        <input type="text" maxlength="500" required name="txtCommentContent" id="txtCommentContent"
                               class="form-control"
                               placeholder="Write a comment">
                    </div>
                    <input type="submit" name="btnAction" value="PostComment" class="btn btn-outline-primary mx-2">
                </form>
            </div>
        </c:if>
        <c:if test="${requestScope.comments.size() > 0}">
            <p>There are ${requestScope.comments.size()} comments</p>

            <div class="list-group">

                <c:forEach items="${requestScope.comments}" var="comment">
                    <div class="list-group-item">
                        <div class="article-comment-title">${comment.userId.fullName}</div>
                        <div class="article-comment-content">${comment.commentContent}</div>
                        <div class="article-comment-date small">${comment.date}</div>
                        <c:if test="${comment.userId.userId == sessionScope.user.userId || sessionScope.user.roleId.roleId == 1}">
                            <button class="btn btn-warning"
                                    onclick="showDeleteCommentConfirmationDialog('${article.articleId}', '${comment.commentId}')">
                                <i class="fa fa-remove"></i>Delete
                            </button>
                        </c:if>
                    </div>
                </c:forEach>

            </div>
        </c:if>
    </div>
</c:if>
<c:if test="${requestScope.article == null}">
    <c:redirect url="error.jsp"/>
</c:if>
</body>
</html>
