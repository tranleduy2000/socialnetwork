<%--
    Document   : createAccount
    Created on : Sep 19, 2020, 12:46:53 AM
    Author     : duy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Create Account Page</title>
    <jsp:include page="metadata.jsp"/>
</head>
<body>
<nav class="navbar navbar-light bg-light">
    <span class="navbar-brand h1">Create new account</span>
</nav>
<div class="container">
    <form method="post" action="MainController">
        <div class="form-group">
            <label>Full name
                <input name="txtFullName" maxlength="50" minlength="2" required value="${param.txtFullName}"
                       class="form-control">
            </label>
        </div>
        <div class="alert-danger">${requestScope.error.fullNameError}</div>
        <div class="form-group">
            <label>Email
                <input name="txtUserId" maxlength="50" required type="email" class="form-control"
                       value="${param.txtUserId}">
            </label>
        </div>
        <div class="alert-danger">${requestScope.error.userIdError} </div>
        <div class="form-group">
            <label>Password
                <input type="password" name="txtPassword" maxlength="20" minlength="2" required
                       class="form-control">
            </label>
        </div>
        <div class="alert-danger"> ${requestScope.error.passwordError}</div>
        <div class="form-group">
            <label>Retype password
                <input type="password" name="txtRePassword" maxlength="20" minlength="2" required
                       class="form-control">
            </label>
        </div>
        <div class="alert-danger">${requestScope.error.rePasswordError}</div>
        <input type="submit" name="btnAction" value="Create Account" class="btn btn-primary">
        <input type="reset" class="btn btn-warning">
    </form>
    <a href="login.jsp">Already has an account? Login</a>
</div>
</body>
</html>
