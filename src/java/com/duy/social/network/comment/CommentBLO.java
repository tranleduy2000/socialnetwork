package com.duy.social.network.comment;

import com.duy.social.network.jpa.controller.CommentJpaController;
import com.duy.social.network.jpa.entity.Article;
import com.duy.social.network.jpa.entity.Comment;
import com.duy.social.network.jpa.entity.Users;
import com.duy.social.network.user.blo.UserBLO;
import com.duy.social.network.utils.DbUtils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.Date;
import java.util.List;

public class CommentBLO {

    private final EntityManagerFactory entityManagerFactory = DbUtils.getInstance();

    public Comment insert(Users user, Article article, String content) {
        try {
            Comment comment = new Comment(0, content, new Date(System.currentTimeMillis()), true);
            comment.setArticleId(article);
            comment.setUserId(user);

            CommentJpaController commentJpaController = new CommentJpaController(entityManagerFactory);
            commentJpaController.create(comment);
            return comment;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean deactive(Users user, int commentId) {
        try {
            CommentJpaController commentJpaController = new CommentJpaController(entityManagerFactory);
            Comment comment = commentJpaController.findComment(commentId);
            if (comment.getUserId().getUserId().equalsIgnoreCase(user.getUserId())
                    || user.getRoleId().getRoleId().equals(UserBLO.ADMIN_ROLE)) {
                comment.setStatus(false);
                commentJpaController.edit(comment);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public List<Comment> getActiveCommentsOfArticleSortByDate(Article article) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            return entityManager.createQuery("SELECT c FROM Comment c " +
                    "WHERE c.status = true AND c.articleId = :articleId " +
                    "ORDER BY c.date ASC", Comment.class)
                    .setParameter("articleId", article)
                    .getResultList();
        } finally {
            entityManager.close();
        }
    }

    public Comment get(Integer commentId) {
        try {
            CommentJpaController commentJpaController = new CommentJpaController(entityManagerFactory);
            return commentJpaController.findComment(commentId);
        } catch (Exception e) {
            return null;
        }
    }
}
