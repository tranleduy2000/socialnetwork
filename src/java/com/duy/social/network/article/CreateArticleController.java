/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duy.social.network.article;

import com.duy.social.network.jpa.entity.Article;
import com.duy.social.network.jpa.entity.Users;
import com.duy.social.network.utils.Constants;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author duy
 */
public class CreateArticleController extends HttpServlet {

    private static final AtomicLong counter = new AtomicLong(System.currentTimeMillis());

    private static final String ERROR_PAGE = "error.jsp";
    private static final String ARTICLE = ArticleController.class.getSimpleName();


    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        String url = ERROR_PAGE;
        try {
            HttpSession session = request.getSession(true);
            if (session.getAttribute("user") == null) {
                url = Constants.LOGIN_PAGE;

            } else {
                Users user = (Users) session.getAttribute("user");

                List<FileItem> fileItems = parseRequest(request);

                String title = getParameter(fileItems, "txtTitle");
                String description = getParameter(fileItems, "txtDescription");
                String imagePath = getParameter(fileItems, "txtImageFile");

                Article article = new Article(0, title, new Date(System.currentTimeMillis()), ArticleBLO.ACTIVE_STATUS);
                article.setDescription(description);
                article.setImagePath(imagePath);
                article.setUserId(user);
                ArticleBLO articleBLO = new ArticleBLO();
                if (articleBLO.insert(article)) {
                    url = ARTICLE;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        response.sendRedirect(url); // do not allow user repeats this action
    }

    /**
     * @return If parameter is a normal format, return its values. If parameter is a file, return relative path of this file.
     */
    private String getParameter(List<FileItem> fileItems, String parameterName) throws Exception {
        for (FileItem fileItem : fileItems) {
            if (fileItem.isFormField()) { // normal parameter
                if (fileItem.getFieldName().equals(parameterName)) {
                    return fileItem.getString();
                }
            } else {  // binary parameter
                if (fileItem.getFieldName().equalsIgnoreCase(parameterName)) {
                    return getFileFromClient(fileItem);
                }
            }
        }
        return null;
    }

    private List<FileItem> parseRequest(HttpServletRequest request) throws FileUploadException {
        final int oneMegabytes = 1024 * 1024;
        final int maxFileSize = 10 * oneMegabytes;
        final int maxMemSize = 4 * oneMegabytes;

        DiskFileItemFactory factory = new DiskFileItemFactory();
        // maximum size that will be stored in memory
        factory.setSizeThreshold(maxMemSize);
        // Location to save data that is larger than maxMemSize.
        File repository = new File(getServletContext().getRealPath("") + "/temp/");
        repository.mkdirs();
        factory.setRepository(repository);
        // Create a new file upload handler
        ServletFileUpload upload = new ServletFileUpload(factory);
        // maximum file size to be uploaded.
        upload.setSizeMax(maxFileSize);

        // Parse the request to get file items.
        return upload.parseRequest(request);
    }

    /**
     * externalPath: the directory outside tomcat webapps directory
     * contextPath: current webapps directory
     *
     * @return relative path.
     * Example:
     * - ExternalPath: C:\Webapp\
     * - Absolute path of a file: C:\Webapp\Images\img1.png
     * - Relative path: \Images\img1.png
     */
    private String getFileFromClient(FileItem fileItem) throws Exception {
        if (fileItem.getSize() <= 0) { // user does not select any file
            return null;
        }
        //  The directory outside tomcat webapps directory
        String externalPath = getServletContext().getInitParameter("external-path");
        String contextPath = getServletContext().getContextPath();

        String fileName = fileItem.getName();
        String fileExt = getExt(fileName, "jpg");
        String hashedName = DigestUtils.sha256Hex(fileName) + counter.getAndIncrement() + "." + fileExt;
        final String relativePath = "/images/" + hashedName;

        File uploadedFile = new File(externalPath, contextPath + "/" + relativePath);
        uploadedFile.getParentFile().mkdirs();
        log("Upload file to: " + uploadedFile.getAbsolutePath());
        fileItem.write(uploadedFile);
        return relativePath;
    }

    private String getExt(String fileName, String defaultVal) {
        if (fileName.contains(".")) {
            return fileName.substring(fileName.lastIndexOf(".") + 1);
        }
        return defaultVal;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
