/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duy.social.network.user;

import com.duy.social.network.user.blo.UserBLO;
import com.duy.social.network.user.dto.UserErrorDTO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author duy
 */
public class UpdatePasswordController extends HttpServlet {

    private static final String LOGIN_PAGE = "login.jsp";
    private static final String UPDATE_PASSWORD_PAGE = "updatePassword.jsp";
    private static final String ERROR_PAGE = "error.jsp";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String url = ERROR_PAGE;
        try {
            final String userId = request.getParameter("txtUserId");
            final String oldPassword = request.getParameter("txtPassword");
            final String rePassword = request.getParameter("txtRePassword");
            final String newPassword = request.getParameter("txtNewPassword");

            boolean error = false;
            UserErrorDTO userErrorDTO = new UserErrorDTO();

            if (newPassword.length() < 2 || newPassword.length() > 20) {
                error = true;
                userErrorDTO.setNewPasswordError("Password length must be from 2 to 12 characters");
            }

            if (!newPassword.equals(rePassword)) {
                error = true;
                userErrorDTO.setRePasswordError("Passwords do not match");
            }

            if (error) {
                request.setAttribute("error", userErrorDTO);
                url = UPDATE_PASSWORD_PAGE;

            } else {
                UserBLO userBLO = new UserBLO();
                if (userBLO.updatePassword(userId, oldPassword, newPassword)) {
                    request.getSession(true).invalidate();
                    url = LOGIN_PAGE;

                } else {
                    userErrorDTO.setPasswordError("Wrong password");
                    request.setAttribute("error", userErrorDTO);
                    url = UPDATE_PASSWORD_PAGE;

                }
            }
        } catch (Exception e) {
            log(e.getMessage(), e);
        }
        request.getRequestDispatcher(url).forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
